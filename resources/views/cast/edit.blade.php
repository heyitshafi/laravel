@extends('layout.master')
@section('judul')
    Edit Cast{{$cast->nama}}
@endsection

@section('isi')
<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" value="{{$cast->nama}}" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur Cast</label>
      <input type="text" value="{{$cast->umur}}" name="umur" class="form-control">
    </div>  
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio Cast</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection
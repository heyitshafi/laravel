@extends('layout.master')
@section('judul')
    Sign Up
@endsection

@section('isi')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="namadepan"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="namabelakang"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection